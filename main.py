# Importation du module "randint" pour générer des nombres aléatoires
import time
from random import randint
import os
import datetime

# Nom du fichier scoreboard
NOM_FICHIER = "scoreboard.txt"

def ask_name():
    """Demande le nom d'utilisateur et le stocke dans une variable current_user."""
    global current_user
    current_user = input("Veuillez entrer votre Pseudo : \n")
    print(f"     Bienvenue {current_user} dans Le Juste Prix !\n")
    return current_user

def check_make_file():
    """Vérifie si le fichier scoreboard.txt existe et le crée s'il n'existe pas."""
    if not os.path.exists(NOM_FICHIER):
        # Le fichier n'existe pas, donc le créer
        with open(NOM_FICHIER, "w") as fichier:
            fichier.write("Liste des Scores Le Juste Prix : \n")
            fichier.write("================================= \n")
        print("Le fichier scoreboard.txt a été créé avec succès.\n")
    else:
        # Le fichier existe déjà
        print("Le fichier scoreboard.txt existe déjà.\n")

def game_historique():
    """Affiche le contenu du fichier scoreboard.txt."""
    scoreboard = open("scoreboard.txt", "r")
    print(scoreboard.read())
    print("")

def game_menu():
    """Affiche le menu du jeu et gère les choix de l'utilisateur."""
    while True:
        print("         ███+>>██ Menu du Jeu ██<<-███\n")
        print("████████████████████████████████████████████████")
        print("█████ * Lancer le jeu :      Appuyer sur P █████")
        print("█████ * Voir le Scoreboard : Appuyer sur S █████")
        print("█████ * Changer de Joueur :  Appuyer sur C █████")
        print("█████ * Arrêter le jeu :     Appuyer sur A █████")
        print("████████████████████████████████████████████████\n")

        menu_choix = input("Votre choix ? \n").lower()

        if 'p' in menu_choix:
            lejusteprix()
        elif 's' in menu_choix:
            game_historique()
        elif 'c' in menu_choix:
            ask_name()
        elif 'a' in menu_choix:
            break
        else:
            print("Choix invalide. Réessayez.\n")

def lejusteprix():
    """Le jeu du Juste Prix."""
    global current_user  # Rend current_user global pour qu'il puisse être utilisé en dehors de la fonction

    # Démarrer le chronomètre
    timer_start = datetime.datetime.now()

    # Choisir un nombre aléatoire entre 1 et 1000
    counter_try = 0
    just_price = randint(1, 1000)

    # État du jeu (actif/inactif)
    running = True

    # Tant que le jeu est en cours
    while running:
        # Demander à l'utilisateur d'entrer un prix
        user_price = int(input("\nEntrer un prix : \n"))

        # Vérifier si la supposition de l'utilisateur est correcte
        if user_price == just_price:
            # Arrêter le chronomètre et calculer le temps écoulé en secondes
            timer_end = datetime.datetime.now()
            time_elapsed = timer_end - timer_start
            total_seconds = int(time_elapsed.total_seconds())

            # Afficher le résultat de l'utilisateur
            print(f"Félicitations {current_user}, vous avez trouvé le juste prix !")
            print(f"Nombre d'essais : {counter_try} Temps : {total_seconds} secondes")

            # Ajouter le résultat de l'utilisateur au fichier scoreboard.txt
            with open(NOM_FICHIER, "a") as fichier:
                fichier.write(f"{current_user}: Nombre d'essais = {counter_try}, Temps (s) = {total_seconds}\n")

            # Fin du jeu
            running = False
            game_menu()

        # Si la supposition de l'utilisateur est supérieure au juste prix
        elif user_price > just_price:
            print("\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< !")
            print(f"<<< C'est moins que {user_price} <<< !")
            print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< !\n")
            counter_try += 1

        # Si la supposition de l'utilisateur est inférieure au juste prix
        elif user_price < just_price:
            print("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> !")
            print(f">>> C'est plus que {user_price}>>> !")
            print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> !\n")
            counter_try += 1

    # Fin du jeu
    print("Fin du jeu !")

# Appeler les fonctions pour exécuter le jeu
check_make_file()
ask_name()
game_menu()
